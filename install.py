#!/usr/bin/python3
import argparse
import filecmp
import os
import shutil
from typing import List, Union

IGNORE_DIR = {'.idea', '.git'}
IGNORE_FILE = {'.gitignore'}


def split_path(path: Union[bytes, str, os.PathLike]) -> List[str]:
    head, tail = os.path.split(path)
    if head == '':
        return [tail]
    else:
        return split_path(head) + [tail]


def update_file(
        dest_file: Union[bytes, str, os.PathLike],
        source_file: Union[bytes, str, os.PathLike],
        dry_run: bool = False
):
    os.makedirs(os.path.dirname(dest_file), exist_ok=True)
    print(f'Installing {dest_file}...')
    if os.path.isfile(dest_file):
        if filecmp.cmp(dest_file, source_file, False):
            print(f'Skipping {dest_file}, file contents are identical!')
        else:
            print(f'File already exists, creating backup at {dest_file}.oldfile')
            if not dry_run:
                os.rename(dest_file, f'{dest_file}.oldfile')
                shutil.copyfile(source_file, dest_file)
    elif not dry_run:
        shutil.copyfile(source_file, dest_file)


def install(args: argparse.Namespace, *, script_path: str):
    script_filename = os.path.basename(script_path)
    dotfiles_dir = os.path.dirname(script_path)
    home_dir = os.getenv('HOME')
    print(f'Installing dotfiles in directory {home_dir}.')
    for current_dir, child_dirs, child_files in os.walk(dotfiles_dir):
        relative_path = os.path.relpath(current_dir, dotfiles_dir)
        split_first = split_path(relative_path)[0]
        if split_first in IGNORE_DIR:
            continue
        for filename in child_files:
            if filename == script_filename or filename in IGNORE_FILE:
                continue
            dotfiles_file = os.path.join(current_dir, filename)
            home_file = os.path.normpath(os.path.join(home_dir, relative_path, filename))
            update_file(home_file, dotfiles_file, dry_run=args.dry)
    print('Installation complete!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Install dotfiles to your $HOME directory.'
    )
    parser.add_argument('--dry', action='store_true')
    install(parser.parse_args(), script_path=__file__)
