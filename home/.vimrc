syntax on
set number
set relativenumber
set autoindent

set expandtab
set tabstop=4
set shiftwidth=4

autocmd Filetype make setlocal noexpandtab

filetype plugin indent on
