#!/usr/bin/env zsh

# Shellcheck directives
# shellcheck disable=SC1090  # ZSH references external files we don't care about
# shellcheck disable=SC2034  # ZSH uses 'unused' variables

# Run script to be executed on init (if user wants to override some behaviour perhaps)
if [[ -f ~/.zshrc.init.local ]]; then
    source ~/.zshrc.init.local
fi

function ryzsh_resolve_bin() {
    echo "$1" # Stub
}

if [[ -f ~/dotfiles/oh-ry-zsh/init.sh ]]; then
    source ~/dotfiles/oh-ry-zsh/init.sh
fi

if [[ "$TERM" != "screen" && "$(echo "$SSH_CLIENT" | awk '{print $1}')" != '127.0.0.1' && -z "$TMUX_PANE" && "$TERMINAL_EMULATOR" != "JetBrains-JediTerm" && -z "$NO_TMUX" ]]; then
    if command -v tmux_session >> /dev/null; then
        exec tmux_session
    fi
fi

# Path to your oh-my-zsh installation.
export ZSH="${HOME}/.oh-my-zsh"

if [[ ! -d "$ZSH" ]]; then
    if [[ "$ZSH_IS_RY" ]]; then
        echo -n "Oh My Zsh not found, install? [y/N] "
        read -r -k 1 choice
        if [[ "${(L)choice}" == "y" ]]; then
            install_ohmyzsh
        else
            echo "Not installing Oh My Zsh."
        fi
    else
        echo "Could not install oh-my-zsh, oh-ry-zsh scripts not found"
    fi
fi

ZSH_THEME="robbyrussell"



# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
# zstyle ':omz:update' mode auto      # update automatically without asking
zstyle ':omz:update' mode reminder    # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
zstyle ':omz:update' frequency 7

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(colorize docker docker-compose git npm python tmux vi-mode zsh-autosuggestions)

# Source early local config
# (occurs here so ZSH config can be changed)
if [[ -f ~/.zshrc.early.local ]]; then
    source ~/.zshrc.early.local
fi

if [[ -f "${ZSH}/oh-my-zsh.sh" ]]; then
    source "${ZSH}/oh-my-zsh.sh"
fi

# User configuration

# You may need to manually set your language environment
export LANG=en_AU.UTF-8

export EDITOR='vim'

export COMPOSER_MEMORY_LIMIT=-1
export NVM_SYMLINK_CURRENT=true


# Environment variables
if [[ -f ~/.environment ]]; then
    # shellcheck source=home/.environment
    source ~/.environment
fi

# More setup
if [[ -f /usr/share/nvm/init-nvm.sh ]]; then
    source /usr/share/nvm/init-nvm.sh
fi

# Source local config
if [[ -f ~/.zshrc.local ]]; then
    source ~/.zshrc.local
fi
