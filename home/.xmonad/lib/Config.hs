module Config where

import Data.Map (Map)
import System.Exit (exitWith, ExitCode (ExitSuccess))
import XMonad (MonadIO, kill, io, windows)
import XMonad.Actions.Warp (warpToScreen)
import qualified XMonad.StackSet as W

import Util.CustomApps (CustomApp, NewCustomApp (Command), define, fromAppList, launchApp)

myBar       = "~/bin/launch_polybar"
myTerminal  = "xst"

customApps :: MonadIO m => Map String (CustomApp (m ()))
customApps = fromAppList apps
    where apps =    [ define $ Command "terminal"           myTerminal
                    , define $ Command "browser"            "google-chrome-stable"
                    , define $ Command "lockScreen"         "i3lock --blur=5 --insidevercolor=55555555 --ringvercolor=aaaaaa55 --linecolor=99999955" 
                    , define $ Command "runCommand"         "~/bin/launcher -show run"
                    , define $ Command "appLauncher"        "~/bin/launcher -show drun"
                    , define $ Command "clipboardManager"   "CM_LAUNCHER=~/bin/launcher clipmenu -- -dmenu"
                    , define $ Command "brightnessUp"       "~/bin/backlight inc"
                    , define $ Command "brightnessDown"     "~/bin/backlight dec"
                    , define $ Command "screenshotRegion"   "flameshot gui"
                    , define $ Command "volumeUp"           "~/bin/volume inc"
                    , define $ Command "volumeDown"         "~/bin/volume dec"
                    , define $ Command "volumeMute"         "~/bin/volume mute"
                    , define $ Command "restartXMonad"      "if type xmonad; then xmonad --recompile && xmonad --restart; else xmessage xmonad not in \\$PATH: \"$PATH\"; fi" 
                    ]

startupApps :: MonadIO m => [CustomApp (m ())]
startupApps = [ define $ Command "compositor"   "~/bin/launch_compositor"
              , define $ Command "wallpaper"    "feh --bg-scale ~/.xmonad/wallpaper.jpg"
              , define $ Command "clipmenud"    "~/bin/launch_clipmenud"
              , define $ Command "volnoti"      "volnoti"
              , define $ Command "ibus-daemon"  "ibus-daemon -drx"
              ]

myFriendlyKeys = [ ("M-b"                       , launchApp "browser"           customApps)
                 , ("M-<Return>"                , launchApp "terminal"          customApps)
                 , ("M-p"                       , launchApp "appLauncher"       customApps)
                 , ("M-S-p"                     , launchApp "runCommand"        customApps)
                 , ("M-v"                , launchApp "clipboardManager"  customApps)
                 , ("C-M1-<Delete>"             , launchApp "lockScreen"        customApps)
                 , ("M-S-q"                     , kill                                    )
                 , ("M-w"                       , warpToScreen 0 0.5 0.5                  )
                 , ("M-f"                       , warpToScreen 1 0.5 0.5                  )
                 , ("M-q"                       , warpToScreen 2 0.5 0.5                  )
                 , ("M-S-<Return>"              , windows W.swapMaster                    ) -- Swaps focused window with master window
                 , ("M-S-<Escape>"              , io (exitWith ExitSuccess)               ) -- Exit xmonad
                 , ("M-S-r"                     , launchApp "restartXMonad"     customApps) -- Restart xmonad
                 , ("<Print>"                   , launchApp "screenshotRegion"  customApps) -- Take screenshot
                 , ("<XF86MonBrightnessUp>"     , launchApp "brightnessUp"      customApps)
                 , ("<XF86MonBrightnessDown>"   , launchApp "brightnessDown"    customApps)
                 , ("<XF86AudioRaiseVolume>"    , launchApp "volumeUp"          customApps)
                 , ("<XF86AudioLowerVolume>"    , launchApp "volumeDown"        customApps)
                 , ("<XF86AudioMute>"           , launchApp "volumeMute"        customApps)
                 ]
