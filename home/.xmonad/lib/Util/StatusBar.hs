{-# LANGUAGE FlexibleContexts, PatternGuards #-}

module Util.StatusBar where

import Codec.Binary.UTF8.String (encodeString, isUTF8Encoded)
import Control.Monad (forM_)
import XMonad
import XMonad.Core 
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks 
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run (hPutStrLn, safeSpawn)
import qualified XMonad.StackSet as S


-- Title logging
titleLogFile = "/tmp/.xmonad-title-log" 

logTitleInfo :: X ()
logTitleInfo = do
    winset <- gets windowset
    wt <- maybe (return "") (fmap show . getName) . S.peek $ winset
    let title = wrap "%{T2}" "%{T-}" wt
    io $ appendFile titleLogFile (title ++ "\n")


-- Workspace logging
workspaceLogFile = "/tmp/.xmonad-workspace-log" 

workspacePP :: PP
workspacePP = def { ppCurrent = wrap "%{B#ddd} " " %{B-}"
                  , ppVisible = wrap " " " "
                  , ppHidden = wrap " " " "
                  , ppHiddenNoWindows = \_ -> ""
                  , ppUrgent = wrap "%{B#fdd} " " %{B-}"
                  , ppSep = ""
                  , ppWsSep = ""
                  , ppTitle = \_ -> ""
                  , ppLayout = \_ -> ""
                  }

logWorkspaceInfo :: X ()
logWorkspaceInfo = do
    wsStr <- dynamicLogString workspacePP
    io $ appendFile workspaceLogFile (wsStr ++ "\n")
    

-- Tying it all together
logBarInfo :: X ()
logBarInfo = do
    logTitleInfo
    logWorkspaceInfo

customStatusBar :: LayoutClass l Window
                => String -- ^ The command to run the status bar
                -> String -- ^ The (emacs-style) key combo to toggle visibility
                -> XConfig l
                -> IO (XConfig (ModifiedLayout AvoidStruts l))
customStatusBar cmd keyCombo conf = do
    safeSpawn "mkfifo" [titleLogFile]
    safeSpawn "mkfifo" [workspaceLogFile]
    spawn cmd
    return $ docks $ conf
        { layoutHook = avoidStruts (layoutHook conf)
        , logHook = logHook conf >> logBarInfo
        } `additionalKeysP` [ (keyCombo, sendMessage ToggleStruts) ]
