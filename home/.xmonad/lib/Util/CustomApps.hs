module Util.CustomApps where

import Data.Map (fromList, Map)
import qualified Data.Map as MapLib
import XMonad (MonadIO)
import XMonad.Core (spawn)

data NewCustomApp = App String | Command String String
data CustomApp m = CustomApp String m

define :: MonadIO m => NewCustomApp -> CustomApp (m ())
define (App name) = CustomApp name $ spawn name
define (Command name command) = CustomApp name $ spawn command
defineCustom :: MonadIO m => CustomApp (m ()) -> CustomApp (m ())
defineCustom app = app

identifier :: MonadIO m => CustomApp (m ()) -> String
identifier (CustomApp name _) = name
launch :: MonadIO m => CustomApp (m ()) -> m ()
launch (CustomApp _ launcher) = launcher

fromAppList :: MonadIO m => [CustomApp (m ())] -> Map String (CustomApp (m ()))
fromAppList as = fromList $ fmap makeTuple as
    where makeTuple a = (identifier a, a)

launchApp :: MonadIO m => String -> Map String (CustomApp (m ())) -> m ()
launchApp name appMap = doLaunch $ MapLib.lookup name appMap
    where doLaunch Nothing = return ()
          doLaunch (Just a) = launch a
