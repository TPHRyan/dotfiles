import XMonad hiding (launch)
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.SetWMName
import XMonad.Layout.Spacing
import XMonad.Util.EZConfig (additionalKeysP)

import Config (customApps, startupApps, myBar, myFriendlyKeys, myTerminal)
import Control.Monad (forM_, liftM)
import Data.List (isPrefixOf)
import Data.Maybe (isJust)
import Util.CustomApps (launch)
import Util.StatusBar (customStatusBar)

myStartupHook :: X ()
myStartupHook = do
    forM_ startupApps (\app -> launch app)
    setWMName "LG3D"

myManageHook = composeAll
    [ insertPosition End Newer
    , appName =? "Godot_Engine" --> doFloat
    ]

myLayoutHook = spacingRaw True (Border 10 10 10 10) True (Border 10 10 10 10) True $
               layoutHook def

myConfig = def 
        { 
            modMask             = mod4Mask -- Super
                , terminal      = myTerminal
                , borderWidth   = 0
                , startupHook   = myStartupHook
                , manageHook    = myManageHook
                , layoutHook    = myLayoutHook
        } `additionalKeysP` myFriendlyKeys

main = do
    configWithBar <- customStatusBar myBar "M-S-b" myConfig
    xmonad $ ewmh configWithBar
