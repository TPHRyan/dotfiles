from thefuck.types import Command


def match(command: Command) -> bool:
    return command.script_parts and command.script_parts[0] in ["vim"]


def get_new_command(command: Command) -> str:
    return f"sudoedit {' '.join(command.script_parts[1:])}"


enabled_by_default = True
