#!/bin/bash

_run_tmux_session() {
    if ! raw_session_data=$(tmux list-sessions); then
        raw_session_data=""
    fi
    if ! session=$(echo "${raw_session_data}" | grep -v '(attached)' | cut -d ':' -f1 | head -n1); then
        session=""
    fi

    if [[ "${session}" != "" ]]; then
        exec tmux attach-session -t "${session}"
    else 
        exec tmux new-session -t main
    fi
}

tmux_session() {
    if command -v tmux >> /dev/null; then
        exec _run_tmux_session
    else
        export NO_TMUX=true
        exec "$SHELL"
    fi
}
