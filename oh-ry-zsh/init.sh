#!/usr/bin/env zsh

ohryzsh_dir="$(readlink -f "$(dirname "$0")")"

install_ohmyzsh() {
    echo "Downloading Oh My Zsh install script."
    install_script_contents="$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    echo "Downloaded install script."
    echo -n "Command to view the install script (default: less): "
    read -r view_command
    if [[ -z "$view_command" ]]; then
        echo "$install_script_contents" | less
    else
        echo "$install_script_contents" | ( ${(z)view_command} )
    fi
    echo -n "Do you want to proceed? [y/N] "
    read -r -k 1 confirm
    if [[ ! "${(L)confirm}" == "y" ]]; then
        echo "Not installing Oh My Zsh."
        return
    fi
    echo
    sh -c "${install_script_contents}"
}

ryzsh_resolve_bin() {
    command_name="$1"
    if [[ -f "${HOME}/bin/${command_name}" ]]; then
        echo "${HOME}/bin/${command_name}"
    else
        echo "${HOME}/dotfiles/bin/${command_name}"
    fi
}

source "${ohryzsh_dir}/tmux_session.sh"

export ZSH_IS_RY="oh-ry-zsh"
